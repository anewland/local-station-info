<?php
/*
Plugin Name: Nexstar Stations
Description: Creates posts that are specific for Nexstar stations on the Nexstar.tv site.
Version: 1.0
Author: Andrew Newland
*/

add_action( 'init', 'create_nexstar_station');

function create_nexstar_station() {
    register_post_type( 'markets',
        array(
            'labels' => array(
                'name' => 'Markets',
                'singular_name' => 'Market',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Market',
                'edit' => 'Edit',
                'edit_item' => 'Edit Market',
                'new_item' => 'New Market',
                'view' => 'View',
                'view_item' => 'View Market',
                'search_items' => 'Search Markets',
                'not_found' => 'No Markets found',
                'not_found_in_trash' => 'No Markets found in Trash'
            ),
 
            'public' => true,
            'rewrite' => array( 'slug' => 'markets' ),
            'menu_position' => 15,
            'supports' => array( 'title','revisions','page-attributes' ),
            'taxonomies' => array( '' ),
            'menu_icon' => 'dashicons-location',
            'has_archive' => false
        )
    );
    
    register_post_type( 'stations',
        array(
            'labels' => array(
                'name' => 'Stations',
                'singular_name' => 'Station',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Station',
                'edit' => 'Edit',
                'edit_item' => 'Edit Station',
                'new_item' => 'New Station',
                'view' => 'View',
                'view_item' => 'View Station',
                'search_items' => 'Search Stations',
                'not_found' => 'No Stations found',
                'not_found_in_trash' => 'No Stations found in Trash',
                'parent' => 'Market'
            ),
 
            'public' => true,
            'rewrite' => array( 'slug' => 'stations' ),
            'menu_position' => 15,
            'supports' => array( 'title','excerpt','revisions','thumbnail','page-attributes' ),
            'taxonomies' => array( '' ),
            'menu_icon' => 'dashicons-location',
            'has_archive' => false
        )
    );    
}

add_action( 'admin_init', 'my_admin' );



function my_admin() {
	// Market Meta Boxes
    add_meta_box( 'market_meta_box',
        'Market Details',
        'display_market_meta_box',
        'markets', 'normal', 'high'
    );
	
	// Station Meta Boxes
    add_meta_box( 'station_meta_box',
        'Station Details',
        'display_station_meta_box',
        'stations', 'normal', 'high'
    );
    add_meta_box( 'station_contacts',
        'Station Contacts',
        'display_station_contacts',
        'stations', 'normal', 'high'
    );
    add_meta_box( 'station_location',
        'Station Location',
        'display_station_location',
        'stations', 'normal', 'high'
    );
}

// Market Information
function display_market_meta_box( $market ) {
    $market_website = esc_html( get_post_meta( $market->ID, 'market_website', true ) );
    $market_rank = esc_html( get_post_meta( $market->ID, 'market_rank', true ) );
    $market_profile = esc_html( get_post_meta( $market->ID, 'market_profile', true ) );
    $market_profile_source = esc_html( get_post_meta( $market->ID, 'market_profile_source', true ) );
    $market_note = esc_html( get_post_meta( $market->ID, 'market_note', true ) );
    $market_marker_top = esc_html( get_post_meta( $market->ID, 'market_marker_top', true ) );
    $market_marker_left = esc_html( get_post_meta( $market->ID, 'market_marker_left', true ) );
    
    $population = esc_html( get_post_meta( $market->ID, 'population', true ) );
    $tv_households = esc_html( get_post_meta( $market->ID, 'tv_households', true ) );
    
    ?>
    <div style="overflow: hidden;">
    <table style="width:30%; float:left;">
        <tr>
            <td style="width: 150px;">Market Website</td>
            <td><input type="text" name="market_website" value="<?=$market_website; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px; padding-top:30px;">Market Rank</td>
            <td style="padding-top:30px;"><input type="text" name="market_rank" value="<?=$market_rank; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">Market Profile Source</td>
            <td><input type="text" name="market_profile_source" value="<?=$market_profile_source; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">Market Population</td>
            <td><input type="text" name="population" value="<?=$population; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">Market TV Households</td>
            <td><input type="text" name="tv_households" value="<?=$tv_households; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px; padding-top:30px;">Market Note Number</td>
            <td style="padding-top:30px;"><input type="text" name="market_note" value="<?=$market_note; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px; padding-top:30px;">Marker Top</td>
            <td style="padding-top:30px;"><input type="text" name="market_marker_top" value="<?=$market_marker_top; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">Marker Left</td>
            <td><input type="text" name="market_marker_left" value="<?=$market_marker_left; ?>" /></td>
        </tr>
    </table>
    
    <table style="width:60%; float:right;">
        <tr>
            <td style="vertical-align: top; width: 100px;">Market Profile</td>
            <td><?php
				$content = $market_profile;
				$editor_id = 'market_profile';
				$settings = array( 'quicktags' => array( 'buttons' => 'strong,em,del,ul,ol,li,close' ), 'media_buttons' => false );
				
				wp_editor( $content, $editor_id, $settings );
				
				?>
            </td>
            <!--<td><textarea name="market_profile" style="width:100%; height:200px;"><?=$market_profile; ?></textarea></td>-->
        </tr>
    </table>
    </div>
    <?php
}


// Station Information
function display_station_meta_box( $station ) {
    $station_website = esc_html( get_post_meta( $station->ID, 'station_website', true ) );
    $station_profile = esc_html( get_post_meta( $station->ID, 'station_profile', true ) );
    $station_affiliation = esc_html( get_post_meta( $station->ID, 'station_affliation', true ) );
    $station_psipchannel = esc_html( get_post_meta( $station->ID, 'station_psipchannel', true ) );
    $station_digitalchannel = esc_html( get_post_meta( $station->ID, 'station_digitalchannel', true ) );
    $station_phone = esc_html( get_post_meta( $station->ID, 'station_phone', true ) );
    $station_fax = esc_html( get_post_meta( $station->ID, 'station_fax', true ) );
    $station_status = esc_html( get_post_meta( $station->ID, 'station_status', true ) );
    $active_page = esc_html( get_post_meta( $station->ID, 'active_page', true ) );
    
    ?>
    <div style="overflow: hidden;">
    <table style="width:30%; float:left;">
        <tr>
            <td style="width: 150px">Station Website</td>
            <td><input type="text" name="station_website" value="<?=$station_website; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">PSIP Channel</td>
            <td><input type="text" name="station_psipchannel" value="<?=$station_psipchannel; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">Digital Channel</td>
            <td><input type="text" name="station_digitalchannel" value="<?=$station_digitalchannel; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">Station Affiliation</td>
            <td>
                <select style="width: 100px" name="station_affliation">
	                <option></option>
                <?php
	                $affiliates = array ( 'ABC', 'CBS', 'NBC', 'FOX', 'MeTV', 'IND', 'CW', 'Movies!', 'Bounce', 'The Country Network', 'My Network TV', 'Estrella TV', 'this TV', 'Telemundo', 'GRIT', 'LAFF', 'Escape', 'Decades', 'GetTV', 'Sky Link TV', 'Cozi TV', 'Antenna TV', 'Justice Network', 'Untamed Sports TV', 'Capital OTB TV', 'MundoMax', 'Ion Television', 'Weather', 'FOX / My Network TV', 'CMT', 'LATV' );
	                sort($affiliates);
	                
	                foreach ($affiliates as $aff) {
		               $is_selected = ($station_affiliation == $aff) ? true : false;
		                
		            	echo '<option value="'.$aff.'" '.($is_selected == true ? 'selected' : '').'>'.$aff.'</option>';
		            }
                ?>
                </select>
            </td>
        </tr>
        <tr>
            <td style="width: 150px">Station Phone</td>
            <td><input type="text" name="station_phone" value="<?=$station_phone; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">Station Fax</td>
            <td><input type="text" name="station_fax" value="<?=$station_fax; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">Station Status</td>
            <td>
                <select style="width: 100px" name="station_status">
	                <option></option>
                <?php
	                $status = array ( 'O&amp;O', 'MSA' );
	                sort($status);
	                
	                foreach ($status as $stat) {
		               $is_selected = ($station_status == $stat) ? true : false;
		                
		            	echo '<option value="'.$stat.'" '.($is_selected == true ? 'selected' : '').'>'.$stat.'</option>';
		            }
                ?>
                </select>
            </td>
        </tr>
        <tr>
            <td style="width: 150px">Is Page Active</td>
            <td>
                <select style="width: 100px" name="active_page">
	                <option></option>
                <?php
	                $status = array ( 'Yes', 'No' );
	                sort($status);
	                
	                foreach ($status as $stat) {
		               $is_selected = ($active_page == $stat) ? true : false;
		                
		            	echo '<option value="'.$stat.'" '.($is_selected == true ? 'selected' : '').'>'.$stat.'</option>';
		            }
                ?>
                </select>
            </td>
        </tr>
    </table>
    
    <table style="width:60%; float:right;">
        <tr>
            <td style="vertical-align: top; width: 100px;">Station Profile</td>
            <td><?php
				$content = $station_profile;
				$editor_id = 'station_profile';
				$settings = array( 'quicktags' => array( 'buttons' => 'strong,em,del,ul,ol,li,close' ), 'media_buttons' => false );
				
				wp_editor( $content, $editor_id, $settings );
				
				?>
            </td>
            <!--<td><textarea name="station_profile" style="width:100%; height:200px;"><?=$station_profile; ?></textarea></td>-->
        </tr>
    </table>
    </div>
    <?php
}

// Station Contact Information
function display_station_contacts( $station ) {
    $pos1_title = esc_html( get_post_meta( $station->ID, 'pos1_title', true ) );
    $pos1_name = esc_html( get_post_meta( $station->ID, 'pos1_name', true ) );
    $pos2_title = esc_html( get_post_meta( $station->ID, 'pos2_title', true ) );
    $pos2_name = esc_html( get_post_meta( $station->ID, 'pos2_name', true ) );
    $pos3_title = esc_html( get_post_meta( $station->ID, 'pos3_title', true ) );
    $pos3_name = esc_html( get_post_meta( $station->ID, 'pos3_name', true ) );
    $pos4_title = esc_html( get_post_meta( $station->ID, 'pos4_title', true ) );
    $pos4_name = esc_html( get_post_meta( $station->ID, 'pos4_name', true ) );
    $pos5_title = esc_html( get_post_meta( $station->ID, 'pos5_title', true ) );
    $pos5_name = esc_html( get_post_meta( $station->ID, 'pos5_name', true ) );
    $pos6_title = esc_html( get_post_meta( $station->ID, 'pos6_title', true ) );
    $pos6_name = esc_html( get_post_meta( $station->ID, 'pos6_name', true ) );
    
    ?>
    <table>
        <tr>
            <td style="width: 150px">Position 1</td>
            <td><input type="text" name="pos1_title" value="<?=$pos1_title; ?>" /></td>
            <td style="width:50px;">&nbsp;</td>
            <td><input type="text" name="pos1_name" value="<?=$pos1_name; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">Position 2</td>
            <td><input type="text" name="pos2_title" value="<?=$pos2_title; ?>" /></td>
            <td>&nbsp;</td>
            <td><input type="text" name="pos2_name" value="<?=$pos2_name; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">Position 3</td>
            <td><input type="text" name="pos3_title" value="<?=$pos3_title; ?>" /></td>
            <td>&nbsp;</td>
            <td><input type="text" name="pos3_name" value="<?=$pos3_name; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">Position 4</td>
            <td><input type="text" name="pos4_title" value="<?=$pos4_title; ?>" /></td>
            <td>&nbsp;</td>
            <td><input type="text" name="pos4_name" value="<?=$pos4_name; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">Position 5</td>
            <td><input type="text" name="pos5_title" value="<?=$pos5_title; ?>" /></td>
            <td>&nbsp;</td>
            <td><input type="text" name="pos5_name" value="<?=$pos5_name; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 150px">Position 6</td>
            <td><input type="text" name="pos6_title" value="<?=$pos6_title; ?>" /></td>
            <td>&nbsp;</td>
            <td><input type="text" name="pos6_name" value="<?=$pos6_name; ?>" /></td>
        </tr>
    </table>
    <?php
}

// Station Location Information
function display_station_location( $station ) {
    $station_address = esc_html( get_post_meta( $station->ID, 'station_address', true ) );
    $station_address_plus = esc_html( get_post_meta( $station->ID, 'station_address_plus', true ) );
    $station_city = esc_html( get_post_meta( $station->ID, 'station_city', true ) );
    $station_state = esc_html( get_post_meta( $station->ID, 'station_state', true ) );
    $station_zipcode = esc_html( get_post_meta( $station->ID, 'station_zipcode', true ) );
    $station_lat = esc_html( get_post_meta( $station->ID, 'station_lat', true ) );
    $station_long = esc_html( get_post_meta( $station->ID, 'station_long', true ) );

    ?>
    <div style="overflow: hidden;">
	    <table style="width:50%; float:left;">
	        <tr>
	            <td style="width: 150px">Street Address</td>
	            <td><input type="text" name="station_address" value="<?=$station_address; ?>" /></td>
	        </tr>
	        <tr>
	            <td style="width: 150px">Address Continued</td>
	            <td><input type="text" name="station_address_plus" value="<?=$station_address_plus; ?>" /></td>
	        </tr>
	        <tr>
	            <td style="width: 150px">City</td>
	            <td><input type="text" name="station_city" value="<?=$station_city; ?>" /></td>
	        </tr>
	        <tr>
	            <td style="width: 150px">State</td>
	            <td><input type="text" name="station_state" value="<?=$station_state; ?>" /></td>
	        </tr>
	        <tr>
	            <td style="width: 150px">Zip Code</td>
	            <td><input type="text" name="station_zipcode" value="<?=$station_zipcode; ?>" /></td>
	        </tr>
	        <tr>
	            <td style="width: 150px">Latatude</td>
	            <td><input type="text" name="station_lat" value="<?=$station_lat; ?>" /></td>
	        </tr>
	        <tr>
	            <td style="width: 150px">Longatude</td>
	            <td><input type="text" name="station_long" value="<?=$station_long; ?>" /></td>
	        </tr>
	    </table>
	    
	    <?php if(isset($station_lat) && isset($station_long)): ?>
			
			<iframe src="https://maps.google.com/maps?q=<?=$station_lat?>,<?=$station_long?>&hl=es;z=15&amp;output=embed" width="480" height="200" frameborder="0" style="border:0; float:right;"></iframe>

	    <?php endif; ?>
    </div>
    <?php
}



add_action( 'save_post', 'add_station_fields', 10, 2 );
add_action( 'save_post', 'add_market_fields', 10, 2 );

function add_station_fields( $station_id, $station ) {
    // Check post type for stations
    if ( $station->post_type == 'stations' ) {
        // Store data in post meta table if present in post data
        if ( isset( $_POST['station_website'] ) ) {
            update_post_meta( $station_id, 'station_website', $_POST['station_website'] );
        }
        if ( isset( $_POST['station_profile'] ) ) {
            update_post_meta( $station_id, 'station_profile', $_POST['station_profile'] );
        }
        if ( isset( $_POST['station_affliation'] ) && $_POST['station_affliation'] != '' ) {
            update_post_meta( $station_id, 'station_affliation', $_POST['station_affliation'] );
        }
        if ( isset( $_POST['station_psipchannel'] ) && $_POST['station_psipchannel'] != '' ) {
            update_post_meta( $station_id, 'station_psipchannel', $_POST['station_psipchannel'] );
        }
        if ( isset( $_POST['station_digitalchannel'] ) && $_POST['station_digitalchannel'] != '' ) {
            update_post_meta( $station_id, 'station_digitalchannel', $_POST['station_digitalchannel'] );
        }
        if ( isset( $_POST['station_phone'] ) && $_POST['station_phone'] != '' ) {
            update_post_meta( $station_id, 'station_phone', $_POST['station_phone'] );
        }
        if ( isset( $_POST['station_fax'] ) && $_POST['station_fax'] != '' ) {
            update_post_meta( $station_id, 'station_fax', $_POST['station_fax'] );
        }
        if ( isset( $_POST['station_address'] ) && $_POST['station_address'] != '' ) {
            update_post_meta( $station_id, 'station_address', $_POST['station_address'] );
        }
        if ( isset( $_POST['station_address_plus'] ) ) {
            update_post_meta( $station_id, 'station_address_plus', $_POST['station_address_plus'] );
        }
		if ( isset( $_POST['station_city'] ) && $_POST['station_city'] != '' ) {
            update_post_meta( $station_id, 'station_city', $_POST['station_city'] );
        }
        if ( isset( $_POST['station_state'] ) && $_POST['station_state'] != '' ) {
            update_post_meta( $station_id, 'station_state', $_POST['station_state'] );
        }
        if ( isset( $_POST['station_zipcode'] ) && $_POST['station_zipcode'] != '' ) {
            update_post_meta( $station_id, 'station_zipcode', $_POST['station_zipcode'] );
        }
        if ( isset( $_POST['station_lat'] ) && $_POST['station_lat'] != '' ) {
            update_post_meta( $station_id, 'station_lat', $_POST['station_lat'] );
        }
        if ( isset( $_POST['station_long'] ) && $_POST['station_long'] != '' ) {
            update_post_meta( $station_id, 'station_long', $_POST['station_long'] );
        }
        if ( isset( $_POST['station_status'] ) ) {
            update_post_meta( $station_id, 'station_status', $_POST['station_status'] );
        }
        if ( isset( $_POST['active_page'] ) && $_POST['active_page'] != '' ) {
            update_post_meta( $station_id, 'active_page', $_POST['active_page'] );
        }
        
        if ( isset( $_POST['pos1_title'] ) ) {
            update_post_meta( $station_id, 'pos1_title', $_POST['pos1_title'] );
        }
        if ( isset( $_POST['pos1_name'] ) ) {
            update_post_meta( $station_id, 'pos1_name', $_POST['pos1_name'] );
        }
        if ( isset( $_POST['pos2_title'] ) ) {
            update_post_meta( $station_id, 'pos2_title', $_POST['pos2_title'] );
        }
        if ( isset( $_POST['pos2_name'] ) ) {
            update_post_meta( $station_id, 'pos2_name', $_POST['pos2_name'] );
        }
        if ( isset( $_POST['pos3_title'] ) ) {
            update_post_meta( $station_id, 'pos3_title', $_POST['pos3_title'] );
        }
        if ( isset( $_POST['pos3_name'] ) ) {
            update_post_meta( $station_id, 'pos3_name', $_POST['pos3_name'] );
        }
        if ( isset( $_POST['pos4_title'] ) ) {
            update_post_meta( $station_id, 'pos4_title', $_POST['pos4_title'] );
        }
        if ( isset( $_POST['pos4_name'] ) ) {
            update_post_meta( $station_id, 'pos4_name', $_POST['pos4_name'] );
        }
        if ( isset( $_POST['pos5_title'] ) ) {
            update_post_meta( $station_id, 'pos5_title', $_POST['pos5_title'] );
        }
        if ( isset( $_POST['pos5_name'] ) ) {
            update_post_meta( $station_id, 'pos5_name', $_POST['pos5_name'] );
        }
        if ( isset( $_POST['pos6_title'] ) ) {
            update_post_meta( $station_id, 'pos6_title', $_POST['pos6_title'] );
        }
        if ( isset( $_POST['pos6_name'] ) ) {
            update_post_meta( $station_id, 'pos6_name', $_POST['pos6_name'] );
        }
    }
}

function add_market_fields( $market_id, $market ) {    
    if ( $market->post_type == 'markets' ) {
        // Store data in post meta table if present in post data
        if ( isset( $_POST['market_website'] ) ) {
            update_post_meta( $market_id, 'market_website', $_POST['market_website'] );
        }
        if ( isset( $_POST['market_profile'] ) ) {
            update_post_meta( $market_id, 'market_profile', $_POST['market_profile'] );
        }
        if ( isset( $_POST['market_profile_source'] ) ) {
            update_post_meta( $market_id, 'market_profile_source', $_POST['market_profile_source'] );
        }
        if ( isset( $_POST['market_rank'] ) ) {
            update_post_meta( $market_id, 'market_rank', $_POST['market_rank'] );
        }
        if ( isset( $_POST['market_note'] ) ) {
            update_post_meta( $market_id, 'market_note', $_POST['market_note'] );
        }
        if ( isset( $_POST['market_marker_top'] ) ) {
            update_post_meta( $market_id, 'market_marker_top', $_POST['market_marker_top'] );
        }
        if ( isset( $_POST['market_marker_left'] ) ) {
            update_post_meta( $market_id, 'market_marker_left', $_POST['market_marker_left'] );
        }
        if ( isset( $_POST['population'] ) ) {
            update_post_meta( $market_id, 'population', $_POST['population'] );
        }
        if ( isset( $_POST['tv_households'] ) ) {
            update_post_meta( $market_id, 'tv_households', $_POST['tv_households'] );
        }
    }
}


function my_connection_types() {
	// Make sure the Posts 2 Posts plugin is active.
	if ( !function_exists( 'p2p_register_connection_type' ) )
		return;

	p2p_register_connection_type( array(
		'name' => 'Stations_to_Markets',
		'from' => 'stations',
		'to' => 'markets',
        'sortable' => 'any',
        'cardinality' => 'many-to-one',
		'reciprocal' => true,
	) );
}
add_action( 'wp_loaded', 'my_connection_types' );



// Create Pages (Stations and Map Coverage)
register_activation_hook( __FILE__, 'insert_page' );

function insert_page(){
    // Create stations page
    $stations_list = array(
      'post_title'    => 'Stations',
      'post_name' 	  => 'stations',
      'post_status'   => 'publish',
      'post_author'   => get_current_user_id(),
      'post_type'     => 'page',
      'page_template' => 'template-stationslist.php'
    );

    // Insert the posts into the database
    wp_insert_post( $stations_list, '' );
}



// Stations Template
add_filter( 'template_include', 'include_template_function', 1 );

function include_template_function( $template_path ) {
    if ( get_post_type() == 'stations' ) {
        if ( is_single() ) {
            // checks if the file exists in the theme first,
            // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array ( 'single-stations.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = plugin_dir_path( __FILE__ ) . '/single-stations.php';
            }
        }
    }
    return $template_path;
}


?>