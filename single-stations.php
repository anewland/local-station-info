<?php 
	$market = get_posts( array(
		'connected_type' => 'Stations_to_Markets', // replace with whatever
		'connected_items' => get_the_ID(),
		'suppress_filters' => false,
		'nopaging' => true
	) );
	
	$stationID = get_the_ID();
	$marketID  = $market[0]->p2p_to;
	
	// Collect all the variables
	$website 		 = get_post_meta( $stationID, 'station_website', true );
	$affiliation 	 = get_post_meta( $stationID, 'station_affliation', true );
	$station_profile = get_post_meta( $stationID, 'station_profile', true );
	$address  		 = get_post_meta( $stationID, 'station_address', true );
	$address_plus 	 = get_post_meta( $stationID, 'station_address_plus', true );
	$city 			 = get_post_meta( $stationID, 'station_city', true );
	$state 			 = get_post_meta( $stationID, 'station_state', true );
	$zipcode 		 = get_post_meta( $stationID, 'station_zipcode', true );
	$phone 			 = get_post_meta( $stationID, 'station_phone', true );
	$fax 			 = get_post_meta( $stationID, 'station_fax', true );
	
	$market_name			= get_post_field( 'post_title', $marketID );
	$market_rank	  		= get_post_meta( $marketID, 'market_rank', true );
	$market_profile  		= get_post_meta( $marketID, 'market_profile', true );
	$market_profile_source  = get_post_meta( $marketID, 'market_profile_source', true );
	$genpop					= get_post_meta( $marketID, 'population', true );
	$tv_households			= get_post_meta( $marketID, 'tv_households', true );
	
	$lat  	= get_post_meta( $stationID, 'station_lat', true );
	$long   = get_post_meta( $stationID, 'station_long', true );

	
	$nf = new NumberFormatter("en_US", NumberFormatter::ORDINAL);
	$primnets = array ('NBC', 'CBS', 'ABC', 'FOX', 'CW', 'MyNetworkTV');
	
	
?> 
<script type="text/javascript" src="/wp-content/plugins/Ultimate_VC_Addons/assets/min-js/ultimate.min.js"></script>

<div class="vc_row wpb_row  vc_custom_1450389512007">
	<div class="wpb_column vc_column_container vc_col-sm-12 vc_custom_1450389402798">
		<div class="wpb_wrapper">
			<div class="wpb_text_column wpb_content_element  station-link">
				<div class="wpb_wrapper">
					<p><a href="/stations">Back to Station List</a></p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="wpb_column vc_column_container vc_col-sm-6 vc_custom_1450389694760">
		<div class="wpb_wrapper">
			<div class="wpb_single_image wpb_content_element vc_align_center">
		
				<figure class="wpb_wrapper vc_figure">
					<div class="vc_single_image-wrapper vc_box_border_grey">
						<?php the_post_thumbnail( array( 463, 236 ) ); ?>
					</div>
				</figure>
				
			</div>
			
			<div class="ubtn-ctn-center" style="text-align:center;">
				<a class="ubtn-link ubtn-center ubtn-normal" href="http://www.<?=$website?>" target="_blank">
					<button type="button" class="ubtn ubtn-normal ubtn-no-hover-bg none ubtn-center tooltip-5799139940b2b" data-hover="" data-border-color="" data-bg="#a19065" data-hover-bg="" data-border-hover="" data-shadow-hover="" data-shadow-click="none" data-shadow="" data-shd-shadow="" style="font-family:'Lato';font-weight:bold;border:none;background: #a19065;color: #ffffff;">
					<span class="ubtn-hover" style="background-color:"></span>
					<span class="ubtn-data ubtn-text">VISIT SITE</span></button>
				</a>
			</div>
		</div>
	</div>
	
	<div class="wpb_column vc_column_container vc_col-sm-6 vc_custom_1450389675113">
		<div class="wpb_wrapper">
			<div class="wpb_text_column wpb_content_element  vc_custom_1464122351610 station-profile">
		<div class="wpb_wrapper">
			<p style="margin-bottom:0"><strong>STATION PROFILE</strong></p>
			<p><?=preg_replace('/\n(\s*\n)+/', '</p><p>', $station_profile)?></p>
			
			<p style="margin:30px 0 0;"><strong>MARKET PROFILE</strong></p>
				<p><span style="margin-bottom:10px; display:block; text-transform:inherit; font-weight:400; color:inherit;"><?=$market_name?> is in the <?=$nf->format($market_rank)?> largest DMA in the United States, with a population of approximately <?=$genpop?> and <?=$tv_households?> television households, as reported by Nielsen Media.</span></p>
				<p><?=$market_profile?></p>
				
				<?php if (!empty($market_profile_source)): ?>
					<p style="font-style:italic; font-size:12px; color:#bbb;">Source: <?=$market_profile_source?></p>
				<?php endif; ?>

		</div>
	</div>
	
	<div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1450390053692">
		<div class="wpb_column vc_column_container vc_col-sm-6 vc_custom_1450389913659">
			<div class="wpb_wrapper">
				<div class="wpb_text_column wpb_content_element  station-profile">
					<div class="wpb_wrapper">
						
						<?php if($address || $phone): ?>
						<p><strong>STATION CONTACT INFORMATION</strong><br>
							<?=$address?><br/>
							<?php if($address_plus): ?>
								<?=$address_plus?><br/>
							<?php endif; ?>
							<?=$city?>, <?=$state?> <?=$zipcode?><br/>
							
							<?php if($phone): ?>
								P: <?=$phone?><br/>
							<?php endif; ?>
							
							<?php if($fax): ?>
								F: <?=$fax?><br/>
							<?php endif; ?>
						</p>
						<p><a href="http://www.<?=$website?>" target="_blank"><?=$website?></a></p>						
						<?php endif;?>

					</div>
				</div>
			</div>
		</div>
		
		<div class="wpb_column vc_column_container vc_col-sm-6 vc_custom_1450389921363">
			<div class="wpb_wrapper">
				<div class="wpb_text_column wpb_content_element  station-profile">
					<div class="wpb_wrapper">
						
						<?php if(get_post_meta( $stationID, 'pos1_name', true )): ?>
						<p><strong>MANAGEMENT CONTACT INFORMATION</strong><br>
							<?php if(get_post_meta( $stationID, 'pos1_name', true )):?>
								<?=get_post_meta( $stationID, 'pos1_name', true )?>, <?=get_post_meta( $stationID, 'pos1_title', true )?><br>
							<?php endif; ?>
							<?php if(get_post_meta( $stationID, 'pos2_name', true )):?>
								<?=get_post_meta( $stationID, 'pos2_name', true )?>, <?=get_post_meta( $stationID, 'pos2_title', true )?><br>
							<?php endif; ?>
							<?php if(get_post_meta( $stationID, 'pos3_name', true )):?>
								<?=get_post_meta( $stationID, 'pos3_name', true )?>, <?=get_post_meta( $stationID, 'pos3_title', true )?><br>
							<?php endif; ?>
							<?php if(get_post_meta( $stationID, 'pos4_name', true )):?>
								<?=get_post_meta( $stationID, 'pos4_name', true )?>, <?=get_post_meta( $stationID, 'pos4_title', true )?><br>
							<?php endif; ?>
							<?php if(get_post_meta( $stationID, 'pos5_name', true )):?>
								<?=get_post_meta( $stationID, 'pos5_name', true )?>, <?=get_post_meta( $stationID, 'pos5_title', true )?><br>
							<?php endif; ?>
							<?php if(get_post_meta( $stationID, 'pos6_name', true )):?>
								<?=get_post_meta( $stationID, 'pos6_name', true )?>, <?=get_post_meta( $stationID, 'pos6_title', true )?><br>
							<?php endif; ?>
						</p>
						<?php endif;?>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div></div></div>	
	
	
<!-- MAP -->
<div class="vc_row wpb_row ac-full-width-row" style="margin-left: -97px; margin-right: -97px; width: 1334px;">
	<div id="map-canvas" style="height:625px; margin-top:50px"></div>		

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&signed_in=true"></script>		
	<script type="text/javascript">
		google.maps.event.addDomListener( window, 'load', gmaps_results_initialize );		
		function gmaps_results_initialize() {
			var map = new google.maps.Map( document.getElementById( 'map-canvas' ), {
				zoom: 14,
				center: new google.maps.LatLng( <?=$lat?>, <?=$long?> ),
				disableDefaultUI: true,
				scrollwheel:  false,
				scaleControl: false,
				streetViewControl: false,
				mapTypeControl: false,
				panControl: false,
				zoomControl: false
			});
			map.setOptions({draggable: false});
		}
	</script>
</div>