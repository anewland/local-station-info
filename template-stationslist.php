<?php

/*
Template Name: Stations List
*/

$markets = get_posts( array(
	'post_type' => 'markets',
	'orderby'   => 'meta_value_num',
	'meta_key'  => 'market_rank',
	'order'		=> 'ASC', 
    'posts_per_page'=>-1, 
    'numberposts'=>-1
) );

$stations = get_posts( array(
	'post_type' => 'stations', 
    'posts_per_page'=>-1, 
    'numberposts'=>-1
) );
?>

<!-- Hover effect for coverage map -->
<style>
	<?php foreach ($markets as $m): ?>
		.station.<?=$m->post_name?>:hover .market-info { display: block; }
	<?php endforeach; ?>
</style>

<div id="primary">
    <div id="content" role="main">

    	<!-- Station Map for Desktop -->
    	<div class="nsxt-map-container">
			<div class="nxst-map unmarked">
				
				<!-- Station Lists -->
				
				<?php foreach($markets as $m): ?>
				<?php
					$related = get_posts( array(
						'connected_type' => 'Stations_to_Markets',
						'connected_items' => $m->ID,
						'suppress_filters' => false,
						'nopaging' => true
					) );
				?>
					<div class="station nxst <?=$m->post_name?>" style="top:<?=get_post_meta( $m->ID, 'market_marker_top', true )?>px; left:<?=get_post_meta( $m->ID, 'market_marker_left', true )?>px;">
						<div class="market-info <?=(get_post_meta( $m->ID, 'market_marker_left', true ) > 575 ? 'e' : 'w')?>">
							<p class="dmarank"><?=get_post_meta( $m->ID, 'market_rank', true );?></p>
							<h2><?=$m->post_title?>
								<?php if(get_post_meta( $m->ID, 'market_note', true )):?>
									<sup><?=get_post_meta( $m->ID, 'market_note', true )?></sup>
								<?php endif; ?>
							</h2>
							<p> <!-- Call Letters -->
								<?php 
									print_r($related);
									$numItems = count($related);
									$n = 0;
									foreach ($related as $s) {
										$n++;
										if(get_post_meta( $s->ID, 'active_page', true ) == 'Yes' || strpos($s->post_title, 'D.2') == false) {
											echo $s->post_title;
											if($n == $numItems-1) {
												echo ' &amp; ';
											} elseif($numItems >= 2 && $n !== $numItems) { 
												echo ', '; 
											};
										}
									}
								?>
							</p>
							<p> <!-- Websites -->
								<?php $n = 0;
									foreach ($related as $s) {
										$n++;
										if(get_post_meta( $s->ID, 'station_website', true ) === get_post_meta( $m->ID, 'market_website', true )) {
											if($n <= 1) {
												echo '<a href="http://'.get_post_meta( $m->ID, 'market_website', true ).'" target="_blank">'.get_post_meta( $m->ID, 'market_website', true ).'</a>';
											} else {
												echo '';
											}
										} else {
											echo ' | <a href="http://'.get_post_meta( $s->ID, 'station_website', true ).'" target="_blank">'.get_post_meta( $s->ID, 'station_website', true ).'</a>';
										}
									}
								?>
							</p>
							<div class="affiliates"> <!-- Affilaites -->
							<?php 
								$affils = array();
								
								foreach($related as $s) { 
									$affils[] = get_post_meta( $s->ID, 'station_affliation', true ); 
								}
								$a = array_unique($affils);
								
								foreach ($a as $a => $value) {
									echo '<i class="'.$value.'"></i>';
								}
							?>
							</div >
						</div>
					</div>
				<?php endforeach; ?>
				
			</div>
		</div>

		<!-- Station List -->
		<table class="tablepress tablepress-id-1 dataTable no-footer" role="grid" style="margin-left: 0px; width: 1140px;">
			<thead>
				<tr>
					<th>Market</th>
					<th>Rank<sup>(1)</sup></th>
					<th>Status<sup>(2)</sup></th>
					<th>Station</th>
					<th>Affiliation</th>
					<th>Web Site</th>
					<th>PSIP Ch.</th>
					<th>Digital Ch.</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($markets as $m): ?>
				<?php
					$related = get_posts( array(
						'connected_type' => 'Stations_to_Markets',
						'connected_items' => $m->ID,
						'suppress_filters' => false,
						'nopaging' => true
					) );
				?>
					<tr>
						<td><?=$m->post_title?></td>
						<td><?=get_post_meta( $m->ID, 'market_rank', true );?></td>
						<td>
							<?php foreach ($related as $s):?>
								<?=get_post_meta( $s->ID, 'station_status', true )?><br/>
							<?php endforeach; ?>
						</td>
						<td>
							<?php foreach ($related as $s):?>
								<?php if(get_post_meta( $s->ID, 'active_page', true ) == 'Yes'): ?>
									<a href="/stations/<?=$s->post_name?>"><?=$s->post_title?></a><br/>
								<?php else: ?>
									<?php if (strpos($s->post_title, 'D.2') == true): ?>
										<br/>
									<?php else: ?>
										<?=$s->post_title?><br/>
									<?php endif; ?>
								<?php endif; ?>
							<?php endforeach; ?>
						</td>
						<td>
							<?php foreach ($related as $s):?>
								<?=get_post_meta( $s->ID, 'station_affliation', true )?><br/>
							<?php endforeach; ?>
						</td>
						<td>
							<?php $n = 0;
								foreach ($related as $s) {
									$n++;
									if(get_post_meta( $s->ID, 'station_website', true ) === get_post_meta( $m->ID, 'market_website', true )) {
										if($n <= 1) {
											echo '<a href="http://'.get_post_meta( $m->ID, 'market_website', true ).'" target="_blank">'.get_post_meta( $m->ID, 'market_website', true ).'</a><br/>';
										} else {
											echo '<br/>';
										}
									} else {
										echo '<a href="http://'.get_post_meta( $s->ID, 'station_website', true ).'" target="_blank">'.get_post_meta( $s->ID, 'station_website', true ).'</a><br/>';
									}
								}
							?>
						</td>
						<td>
							<?php foreach ($related as $s):?>
								<?=get_post_meta( $s->ID, 'station_psipchannel', true )?><br/>
							<?php endforeach; ?>
						</td>
						<td>
							<?php foreach ($related as $s):?>
								<?=get_post_meta( $s->ID, 'station_digitalchannel', true )?><br/>
							<?php endforeach; ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
			
		</table>
		
		
		<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>

    </div>
</div>